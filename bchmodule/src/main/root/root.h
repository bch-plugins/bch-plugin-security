#ifndef ROOT_H
#define ROOT_H

struct __jni_env_thread
{
    void **__arr;
    unsigned int __sz;
};

typedef struct __jni_env_thread __env_thread;

#endif /* ROOT_H */
