#include <android/log.h>
#include <dlfcn.h>
#include <sys/mman.h>
#include <pthread.h>
#include <selinux/selinux.h>
#include <time.h>
#include "../magisk/magisk.h"
#include "root.h"

static const char *TAG = "RootLibDetector";
static const int MAX_ROOT_PKGS = 17;
static const int MAX_DANGEROUS_PKGS = 6;
static const int MAX_CLOAKING_PKGS = 10;
static const int MAX_SU_PATHS = 15;
static const int MAX_PATHS_NON_RW = 8;
static int PATHS_TO_CHECK = 0;
static uint8_t __flag = 0;
static unsigned int __qtGl = 0;
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
static uint8_t (*loadfn)(void);
static volatile void * volatile __pmCmd; /* pm list packages (Línea 569 / 600) */
static volatile void * volatile __procMnt; /* proc/self/mounts (Línea 1033) */
static volatile void * volatile __pmCls; /* PackageManager (Línea 850) */
static volatile void * volatile __pmClsDef; /* Landroid/content/pm/PackageManager; (Línea 850) */
static volatile void * volatile __pmGetInstMethod; /* getInstalledApplications (Línea 855) */
static volatile void * volatile __pmGetInstMethodDef; /* (I)Ljava/util/List; (Línea 855) */
static volatile void * volatile __sizeL; /* size (Línea 860) */
static volatile void * volatile __sizeLDef; /* ()I (Línea 860) */
static volatile void * volatile __getFromL; /* get (Línea 866) */
static volatile void * volatile __getFromLDef; /* (I)Ljava/lang/Object; (Línea 866 */
static volatile void * volatile __appInfoCls; /* android/content/pm/ApplicationInfo (Línea 869) */
static volatile void * volatile __pkgN; /* packageName (Línea 873) */
static volatile void * volatile __pkgNDef; /* Ljava/lang/String; */
static volatile char ** volatile __uapps;
static volatile void * volatile dl;
static volatile void * volatile ld;
static volatile void ** volatile rootPkgs;
static volatile void ** volatile dangerousPkgs;
static volatile void ** volatile cloakingPkgs;
static volatile void ** volatile suPaths;
static volatile void ** volatile nonRwPaths;

static inline void *rootPkgsAnalyzer(void *);
static inline void *dangerousPkgsAnalyzer(void *);
static inline void *cloakingPkgsAnalyzer(void *);
static inline int getPipeSize();
static inline uint8_t checkPermissiveness(void);
static inline uint8_t checkSuPaths(void);
static inline uint8_t checkForRwMounts(void);
static inline uint8_t checkPresence(const char *);
static inline char **mountProcess(void);

__attribute__((constructor(100), always_inline))
static inline void rootCtor(void)
{
    static void *dlName = NULL;
    static void *ldName = NULL;

    __android_log_print(ANDROID_LOG_INFO, TAG, "Ctor1");

    dlName = mmap(0, sizeof(char) * 13, PROT_READ | PROT_WRITE,
                  MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
    ldName = mmap(0, sizeof(char) * 5, PROT_READ | PROT_WRITE,
            MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
    memcpy(dlName, (void *)(char []) { 0x6C, 0x69, 0x62, 0x6D, 0x61, 0x67, 0x69, 0x73, 0x6B, 0x2E, 0x73, 0x6F }, 12);
    memcpy(ldName, (void *)(char []) { 0x6C, 0x6F, 0x61, 0x64 }, 4);
    *(char *)&dlName[12] = ((char)0);
    *(char *)&ldName[4] = ((char)0);
    dl = (volatile void * volatile)dlName;
    ld = (volatile void * volatile)ldName;
}

__attribute__((constructor(101), always_inline))
static inline void rootCtor2(void)
{
    static void **rootpkgs = NULL;

    rootpkgs = mmap(0, sizeof(char *) * MAX_ROOT_PKGS, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
    for (int i = 0; i < MAX_ROOT_PKGS; i++)
    {
        switch (i)
        {
            case 0:
            {
                rootpkgs[i] = mmap(0, sizeof(char) * 24, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(rootpkgs[i], (void *)(char []) { 0x63, 0x6F, 0x6D, 0x2E, 0x6E, 0x6F, 0x73, 0x68, 0x75, 0x66, 0x6F, 0x75, 0x2E, 0x61, 0x6E, 0x64, 0x72, 0x6F, 0x69, 0x64, 0x2E, 0x73, 0x75 }, 23);
                *(char *)&rootpkgs[i][23] = ((char)0);
                break;
            }
            case 1:
            {
                rootpkgs[i] = mmap(0, sizeof(char) * 30, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(rootpkgs[i], (void *)(char []) { 0x63, 0x6F, 0x6D, 0x2E, 0x6E, 0x6F, 0x73, 0x68, 0x75, 0x66, 0x6F, 0x75, 0x2E, 0x61, 0x6E, 0x64, 0x72, 0x6F, 0x69, 0x64, 0x2E, 0x73, 0x75, 0x2E, 0x65, 0x6C, 0x69, 0x74, 0x65 }, 29);
                *(char *)&rootpkgs[i][29] = ((char)0);
                break;
            }
            case 2:
            {
                rootpkgs[i] = mmap(0, sizeof(char) * 21, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(rootpkgs[i], (void *)(char []) { 0x65, 0x75, 0x2E, 0x63, 0x68, 0x61, 0x69, 0x6E, 0x66, 0x69, 0x72, 0x65, 0x2E, 0x73, 0x75, 0x70, 0x65, 0x72, 0x73, 0x75 }, 20);
                *(char *)&rootpkgs[i][20] = ((char)0);
                break;
            }
            case 3:
            {
                rootpkgs[i] = mmap(0, sizeof(char) * 27, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(rootpkgs[i], (void *)(char []) { 0x63, 0x6F, 0x6D, 0x2E, 0x6B, 0x6F, 0x75, 0x73, 0x68, 0x69, 0x6B, 0x64, 0x75, 0x74, 0x74, 0x61, 0x2E, 0x73, 0x75, 0x70, 0x65, 0x72, 0x75, 0x73, 0x65, 0x72 }, 26);
                *(char *)&rootpkgs[i][26] = ((char)0);
                break;
            }
            case 4:
            {
                rootpkgs[i] = mmap(0, sizeof(char) * 25, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(rootpkgs[i], (void *)(char []) { 0x63, 0x6F, 0x6D, 0x2E, 0x74, 0x68, 0x69, 0x72, 0x64, 0x70, 0x61, 0x72, 0x74, 0x79, 0x2E, 0x73, 0x75, 0x70, 0x65, 0x72, 0x75, 0x73, 0x65, 0x72 }, 24);
                *(char *)&rootpkgs[i][24] = ((char)0);
                break;
            }
            case 5:
            {
                rootpkgs[i] = mmap(0, sizeof(char) * 16, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(rootpkgs[i], (void *)(char []) { 0x63, 0x6F, 0x6D, 0x2E, 0x79, 0x65, 0x6C, 0x6C, 0x6F, 0x77, 0x65, 0x73, 0x2E, 0x73, 0x75 }, 15);
                *(char *)&rootpkgs[i][15] = ((char)0);
                break;
            }
            case 6:
            {
                rootpkgs[i] = mmap(0, sizeof(char) * 31, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(rootpkgs[i], (void *)(char []) { 0x63, 0x6F, 0x6D, 0x2E, 0x7A, 0x61, 0x63, 0x68, 0x73, 0x70, 0x6F, 0x6E, 0x67, 0x2E, 0x74, 0x65, 0x6D, 0x70, 0x72, 0x6F, 0x6F, 0x74, 0x72, 0x65, 0x6D, 0x6F, 0x76, 0x65, 0x6A, 0x62 }, 30);
                *(char *)&rootpkgs[i][30] = ((char)0);
                break;
            }
            case 7:
            {
                rootpkgs[i] = mmap(0, sizeof(char) * 27, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(rootpkgs[i], (void *)(char []) { 0x63, 0x6F, 0x6D, 0x2E, 0x72, 0x61, 0x6D, 0x64, 0x72, 0x6F, 0x69, 0x64, 0x2E, 0x61, 0x70, 0x70, 0x71, 0x75, 0x61, 0x72, 0x61, 0x6E, 0x74, 0x69, 0x6E, 0x65 }, 26);
                *(char *)&rootpkgs[i][26] = ((char)0);
                break;
            }
            case 8:
            {
                rootpkgs[i] = mmap(0, sizeof(char) * 21, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(rootpkgs[i], (void *)(char []) { 0x63, 0x6F, 0x6D, 0x2E, 0x74, 0x6F, 0x70, 0x6A, 0x6F, 0x68, 0x6E, 0x77, 0x75, 0x2E, 0x6D, 0x61, 0x67, 0x69, 0x73, 0x6B }, 20);
                *(char *)&rootpkgs[i][20] = ((char)0);
                break;
            }
            case 9:
            {
                rootpkgs[i] = mmap(0, sizeof(char) * 33, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(rootpkgs[i], (void *)(char []) { 0x63, 0x6F, 0x6D, 0x2E, 0x65, 0x6C, 0x64, 0x65, 0x72, 0x64, 0x72, 0x69, 0x76, 0x65, 0x72, 0x73, 0x2E, 0x72, 0x69, 0x72, 0x75, 0x2E, 0x65, 0x64, 0x78, 0x70, 0x2E, 0x79, 0x61, 0x68, 0x66, 0x61 }, 32);
                *(char *)&rootpkgs[i][32] = ((char)0);
                break;
            }
            case 10:
            {
                rootpkgs[i] = mmap(0, sizeof(char) * 33, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(rootpkgs[i], (void *)(char []) { 0x63, 0x6F, 0x6D, 0x2E, 0x65, 0x6C, 0x64, 0x65, 0x72, 0x64, 0x72, 0x69, 0x76, 0x65, 0x72, 0x73, 0x2E, 0x72, 0x69, 0x72, 0x75, 0x2E, 0x65, 0x64, 0x78, 0x70, 0x2E, 0x77, 0x68, 0x61, 0x6C, 0x65 }, 32);
                *(char *)&rootpkgs[i][32] = ((char)0);
                break;
            }
            case 11:
            {
                rootpkgs[i] = mmap(0, sizeof(char) * 36, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(rootpkgs[i], (void *)(char []) { 0x63, 0x6F, 0x6D, 0x2E, 0x65, 0x6C, 0x64, 0x65, 0x72, 0x64, 0x72, 0x69, 0x76, 0x65, 0x72, 0x73, 0x2E, 0x72, 0x69, 0x72, 0x75, 0x2E, 0x65, 0x64, 0x78, 0x70, 0x2E, 0x73, 0x61, 0x6E, 0x64, 0x68, 0x6F, 0x6F, 0x6B }, 35);
                *(char *)&rootpkgs[i][35] = ((char)0);
                break;
            }
            case 12:
            {
                rootpkgs[i] = mmap(0, sizeof(char) * 27, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(rootpkgs[i], (void *)(char []) { 0x63, 0x6F, 0x6D, 0x2E, 0x65, 0x6C, 0x64, 0x65, 0x72, 0x64, 0x72, 0x69, 0x76, 0x65, 0x72, 0x73, 0x2E, 0x72, 0x69, 0x72, 0x75, 0x2E, 0x65, 0x64, 0x78, 0x70 }, 26);
                *(char *)&rootpkgs[i][26] = ((char)0);
                break;
            }
            case 13:
            {
                rootpkgs[i] = mmap(0, sizeof(char) * 34, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(rootpkgs[i], (void *)(char []) { 0x63, 0x6F, 0x6D, 0x2E, 0x65, 0x6C, 0x64, 0x65, 0x72, 0x64, 0x72, 0x69, 0x76, 0x65, 0x72, 0x73, 0x2E, 0x72, 0x69, 0x72, 0x75, 0x2E, 0x65, 0x64, 0x78, 0x70, 0x2E, 0x63, 0x6F, 0x6D, 0x6D, 0x6F, 0x6E }, 33);
                *(char *)&rootpkgs[i][33] = ((char)0);
                break;
            }
            case 14:
            {
                rootpkgs[i] = mmap(0, sizeof(char) * 26, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(rootpkgs[i], (void *)(char []) { 0x65, 0x75, 0x2E, 0x63, 0x68, 0x61, 0x69, 0x6E, 0x66, 0x69, 0x72, 0x65, 0x2E, 0x6C, 0x69, 0x62, 0x73, 0x75, 0x70, 0x65, 0x72, 0x75, 0x73, 0x65, 0x72 }, 25);
                *(char *)&rootpkgs[i][25] = ((char)0);
                break;
            }
            case 15:
            {
                rootpkgs[i] = mmap(0, sizeof(char) * 22, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(rootpkgs[i], (void *)(char []) { 0x63, 0x6F, 0x6D, 0x2E, 0x6B, 0x69, 0x6E, 0x67, 0x72, 0x6F, 0x6F, 0x74, 0x2E, 0x6B, 0x69, 0x6E, 0x67, 0x75, 0x73, 0x65, 0x72 }, 21);
                *(char *)&rootpkgs[i][21] = ((char)0);
                break;
            }
            case 16:
            {
                rootpkgs[i] = mmap(0, sizeof(char) * 17, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(rootpkgs[i], (void *)(char []) { 0x63, 0x6F, 0x6D, 0x2E, 0x6B, 0x69, 0x6E, 0x67, 0x6F, 0x61, 0x70, 0x70, 0x2E, 0x61, 0x70, 0x6B }, 16);
                *(char *)&rootpkgs[i][16] = ((char)0);
                break;
            }
            default:
                break;
        }
    }
    rootPkgs = (volatile void ** volatile) rootpkgs;
}

__attribute__((constructor(102), always_inline))
static inline void rootCtor3(void)
{
    static void **dangerousPkgsx = NULL;

    __android_log_print(ANDROID_LOG_INFO, TAG, "Ctor3");
    dangerousPkgsx = mmap(0, sizeof(char *) * MAX_DANGEROUS_PKGS, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
    for (int i = 0; i < MAX_DANGEROUS_PKGS; i++)
    {
        switch (i)
        {
            case 0:
            {
                dangerousPkgsx[i] = mmap(0, sizeof(char) * 28, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(dangerousPkgsx[i], (void *)(char []) { 0x63, 0x6F, 0x6D ,0x2E, 0x6B, 0x6F, 0x75, 0x73, 0x68, 0x69, 0x6B, 0x64, 0x75, 0x74, 0x74, 0x61, 0x2E, 0x72, 0x6F, 0x6D, 0x6D, 0x61, 0x6E, 0x61, 0x67, 0x65, 0x72}, 27);
                *(char *)&dangerousPkgsx[i][27] = ((char)0);
                break;
            }
            case 1:
            {
                dangerousPkgsx[i] = mmap(0, sizeof(char) * 36, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(dangerousPkgsx[i], (void *)(char []) { 0x63, 0x6F, 0x6D, 0x2E, 0x6B, 0x6F, 0x75, 0x73, 0x68, 0x69, 0x6b, 0x64, 0x75, 0x74, 0x74, 0x61, 0x2e, 0x72, 0x6f, 0x6d, 0x6d, 0x61, 0x6e, 0x61, 0x67, 0x65, 0x72, 0x2e, 0x6c, 0x69, 0x63, 0x65, 0x6e, 0x73, 0x65 }, 35);
                *(char *)&dangerousPkgsx[i][35] = ((char)0);
                break;
            }
            case 2:
            {
                dangerousPkgsx[i] = mmap(0, sizeof(char) * 28, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(dangerousPkgsx[i], (void *)(char []) { 0x63, 0x6F, 0x6D, 0x2E, 0x64, 0x69, 0x6D, 0x6F, 0x6E, 0x76, 0x69, 0x64, 0x65, 0x6F, 0x2E, 0x6C, 0x75, 0x63, 0x6B, 0x79, 0x70, 0x61, 0x74, 0x63, 0x68, 0x65, 0x72 }, 27);
                *(char *)&dangerousPkgsx[i][27] = ((char)0);
                break;
            }
            case 3:
            {
                dangerousPkgsx[i] = mmap(0, sizeof(char) * 23, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(dangerousPkgsx[i], (void *)(char []) { 0x63, 0x6F, 0x6D, 0x2E, 0x63, 0x68, 0x65, 0x6C, 0x70, 0x75, 0x73, 0x2E, 0x6C, 0x61, 0x63, 0x6B, 0x79, 0x70, 0x61, 0x74, 0x63, 0x68 }, 22);
                *(char *)&dangerousPkgsx[i][22] = ((char)0);
                break;
            }
            case 4:
            {
                dangerousPkgsx[i] = mmap(0, sizeof(char) * 27, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(dangerousPkgsx[i], (void *)(char []) { 0x63, 0x6F, 0x6D, 0x2E, 0x72, 0x61, 0x6D, 0x64, 0x72, 0x6F, 0x69, 0x64, 0x2E, 0x61, 0x70, 0x70, 0x71, 0x75, 0x61, 0x72, 0x61, 0x6E, 0x74, 0x69, 0x6E, 0x65 }, 26);
                *(char *)&dangerousPkgsx[i][26] = ((char)0);
                break;
            }
            case 5:
            {
                dangerousPkgsx[i] = mmap(0, sizeof(char) * 30, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(dangerousPkgsx[i], (void *)(char []) { 0x63, 0x6F, 0x6D, 0x2E, 0x72, 0x61, 0x6D, 0x64, 0x72, 0x6F, 0x69, 0x64, 0x2E, 0x61, 0x70, 0x70, 0x71, 0x75, 0x61, 0x72, 0x61, 0x6E, 0x74, 0x69, 0x6E, 0x65, 0x70, 0x72, 0x6F}, 29);
                *(char *)&dangerousPkgsx[i][29] = ((char)0);
                break;
            }
            default:
                break;
        }
    }
    dangerousPkgs = (volatile void ** volatile) dangerousPkgsx;
}


__attribute__((constructor(103), always_inline))
static inline void rootCtor4(void)
{
    static void **cloakingPkgsx = NULL;

    cloakingPkgsx = mmap(0, sizeof(char *) * MAX_CLOAKING_PKGS, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
    for (int i = 0; i < MAX_CLOAKING_PKGS; i++)
    {
        switch (i)
        {
            case 0:
            {
                cloakingPkgsx[i] = mmap(0, sizeof(char) * 25, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(cloakingPkgsx[i], (void *)(char []) { 0x63, 0x6F, 0x6D, 0x2E, 0x64, 0x65, 0x76, 0x61, 0x64, 0x76, 0x61, 0x6E, 0x63, 0x65, 0x2E, 0x72, 0x6F, 0x6F, 0x74, 0x63, 0x6C, 0x6F, 0x61, 0x6B}, 24);
                *(char *)&cloakingPkgsx[i][24] = ((char)0);
                break;
            }
            case 1:
            {
                cloakingPkgsx[i] = mmap(0, sizeof(char) * 29, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(cloakingPkgsx[i], (void *)(char []) { 0x63, 0x6F, 0x6D, 0x2E, 0x64, 0x65, 0x76, 0x61, 0x64, 0x76, 0x61, 0x6e, 0x63, 0x65, 0x2E, 0x72, 0x6F, 0x6F, 0x74, 0x63, 0x6C, 0x6F, 0x61, 0x6B, 0x70, 0x6C, 0x75, 0x73 }, 28);
                *(char *)&cloakingPkgsx[i][28] = ((char)0);
                break;
            }
            case 2:
            {
                cloakingPkgsx[i] = mmap(0, sizeof(char) * 33, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(cloakingPkgsx[i], (void *)(char []) { 0x64, 0x65, 0x2E, 0x72, 0x6F, 0x62, 0x76, 0x2E, 0x61, 0x6E, 0x64, 0x72, 0x6F, 0x69, 0x64, 0x2E, 0x78, 0x70, 0x6F, 0x73, 0x65, 0x64, 0x2E, 0x69, 0x6E, 0x73, 0x74, 0x61, 0x6C, 0x6C, 0x65, 0x72 }, 32);
                *(char *)&cloakingPkgsx[i][32] = ((char)0);
                break;
            }
            case 3:
            {
                cloakingPkgsx[i] = mmap(0, sizeof(char) * 21, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(cloakingPkgsx[i], (void *)(char []) { 0x63, 0x6F, 0x6D, 0x2E, 0x73, 0x61, 0x75, 0x72, 0x69, 0x6B, 0x2E, 0x73, 0x75, 0x62, 0x73, 0x74, 0x72, 0x61, 0x74, 0x65 }, 20);
                *(char *)&cloakingPkgsx[i][20] = ((char)0);
                break;
            }
            case 4:
            {
                cloakingPkgsx[i] = mmap(0, sizeof(char) * 31, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(cloakingPkgsx[i], (void *)(char []) { 0x63, 0x6F, 0x6D, 0x2E, 0x7A, 0x61, 0x63, 0x68, 0x73, 0x70, 0x6F, 0x6E, 0x67, 0x2E, 0x74, 0x65, 0x6D, 0x70, 0x72, 0x6F, 0x6F, 0x74, 0x72, 0x65, 0x6D, 0x6F, 0x76, 0x65, 0x6A, 0x62 }, 30);
                *(char *)&cloakingPkgsx[i][30] = ((char)0);
                break;
            }
            case 5:
            {
                cloakingPkgsx[i] = mmap(0, sizeof(char) * 24, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(cloakingPkgsx[i], (void *)(char []) { 0x63, 0x6f, 0x6d, 0x2e, 0x61, 0x6d, 0x70, 0x68, 0x6f, 0x72, 0x61, 0x73, 0x2e, 0x68, 0x69, 0x64, 0x65, 0x6d, 0x79, 0x72, 0x6f, 0x6f, 0x74 }, 23);
                *(char *)&cloakingPkgsx[i][23] = ((char)0);
                break;
            }
            case 6:
            {
                cloakingPkgsx[i] = mmap(0, sizeof(char) * 30, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(cloakingPkgsx[i], (void *)(char []) { 0x63, 0x6F, 0x6D, 0x2E, 0x61, 0x6D, 0x70, 0x68, 0x6F, 0x72, 0x61, 0x73, 0x2E, 0x68, 0x69, 0x64, 0x65, 0x6D, 0x79, 0x72, 0x6F, 0x6F, 0x74, 0x61, 0x64, 0x66, 0x72, 0x65, 0x65 }, 29);
                *(char *)&cloakingPkgsx[i][29] = ((char)0);
                break;
            }
            case 7:
            {
                cloakingPkgsx[i] = mmap(0, sizeof(char) * 28, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(cloakingPkgsx[i], (void *)(char []) { 0x63, 0x6F, 0x6D, 0x2E, 0x66, 0x6F, 0x72, 0x6D, 0x79, 0x68, 0x6D, 0x2E, 0x68, 0x69, 0x64, 0x65, 0x72, 0x6F, 0x6F, 0x74, 0x50, 0x72, 0x65, 0x6D, 0x69, 0x75, 0x6D }, 27);
                *(char *)&cloakingPkgsx[i][27] = ((char)0);
                break;
            }
            case 8:
            {
                cloakingPkgsx[i] = mmap(0, sizeof(char) * 21, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(cloakingPkgsx[i], (void *)(char []) { 0x63, 0x6F, 0x6D, 0x2E, 0x66, 0x6F, 0x72, 0x6D, 0x79, 0x68, 0x6D, 0x2E, 0x68, 0x69, 0x64, 0x65, 0x72, 0x6F, 0x6F, 0x74 }, 20);
                *(char *)&cloakingPkgsx[i][20] = ((char)0);
                break;
            }
            case 9:
            {
                cloakingPkgsx[i] = mmap(0, sizeof(char) * 20, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(cloakingPkgsx[i], (void *)(char []) { 0x65, 0x75, 0x2E, 0x63, 0x68, 0x61, 0x69, 0x6E, 0x66, 0x69, 0x72, 0x65, 0x2E, 0x73, 0x75, 0x68, 0x69, 0x64, 0x65 }, 19);
                *(char *)&cloakingPkgsx[i][19] = ((char)0);
                break;
            }
            default:
                break;
        }
    }
    cloakingPkgs = (volatile void ** volatile) cloakingPkgsx;
}

__attribute__((constructor(104), always_inline))
static inline void rootCtor5(void)
{
    static void **suPathsx = NULL;

    suPathsx = mmap(0, sizeof(char *) * MAX_SU_PATHS, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
    for (int i = 0; i < MAX_SU_PATHS; i++)
    {
        switch (i)
        {
            case 0:
            {
                suPathsx[i] = mmap(0, sizeof(char) * 13, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(suPathsx[i], (void *)(char []) { 0x2F, 0x64, 0x61, 0x74, 0x61, 0x2F, 0x6C, 0x6F, 0x63, 0x61, 0x6C, 0x2F }, 12);
                *(char *)&suPathsx[i][12] = ((char)0);
                break;
            }
            case 1:
            {
                suPathsx[i] = mmap(0, sizeof(char) * 17, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(suPathsx[i], (void *)(char []) { 0x2F, 0x64, 0x61, 0x74, 0x61, 0x2F, 0x6C, 0x6F, 0x63, 0x61, 0x6C, 0x2F, 0x62, 0x69, 0x6E, 0x2F }, 16);
                *(char *)&suPathsx[i][16] = ((char)0);
                break;
            }
            case 2:
            {
                suPathsx[i] = mmap(0, sizeof(char) * 18, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(suPathsx[i], (void *)(char []) { 0x2F, 0x64, 0x61, 0x74, 0x61, 0x2F, 0x6C, 0x6F, 0x63, 0x61, 0x6C, 0x2F, 0x78, 0x62, 0x69, 0x6E, 0x2F }, 17);
                *(char *)&suPathsx[i][17] = ((char)0);
                break;
            }
            case 3:
            {
                suPathsx[i] = mmap(0, sizeof(char) * 7, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(suPathsx[i], (void *)(char []) { 0x2F, 0x73, 0x62, 0x69, 0x6E, 0x2F }, 6);
                *(char *)&suPathsx[i][6] = ((char)0);
                break;
            }
            case 4:
            {
                suPathsx[i] = mmap(0, sizeof(char) * 9, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(suPathsx[i], (void *)(char []) { 0x2F, 0x73, 0x75, 0x2F, 0x62, 0x69, 0x6E, 0x2F }, 8);
                *(char *)&suPathsx[i][8] = ((char)0);
                break;
            }
            case 5:
            {
                suPathsx[i] = mmap(0, sizeof(char) * 13, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(suPathsx[i], (void *)(char []) { 0x2F, 0x73, 0x79, 0x73, 0x74, 0x65, 0x6D, 0x2F, 0x62, 0x69, 0x6E, 0x2F }, 12);
                *(char *)&suPathsx[i][12] = ((char)0);
                break;
            }
            case 6:
            {
                suPathsx[i] = mmap(0, sizeof(char) * 18, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(suPathsx[i], (void *)(char []) { 0x2f, 0x73, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x2f, 0x62, 0x69, 0x6e, 0x2f, 0x2e, 0x65, 0x78, 0x74, 0x2f }, 17);
                *(char *)&suPathsx[i][17] = ((char)0);
                break;
            }
            case 7:
            {
                suPathsx[i] = mmap(0, sizeof(char) * 22, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(suPathsx[i], (void *)(char []) { 0x2F, 0x73, 0x79, 0x73, 0x74, 0x65, 0x6D, 0x2F, 0x62, 0x69, 0x6E, 0x2F, 0x66, 0x61, 0x69, 0x6C, 0x73, 0x61, 0x66, 0x65, 0x2F }, 21);
                *(char *)&suPathsx[i][21] = ((char)0);
                break;
            }
            case 8:
            {
                suPathsx[i] = mmap(0, sizeof(char) * 17, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(suPathsx[i], (void *)(char []) { 0x2F, 0x73, 0x79, 0x73, 0x74, 0x65, 0x6D, 0x2F, 0x73, 0x64, 0x2F, 0x78, 0x62, 0x69, 0x6E, 0x2F }, 16);
                *(char *)&suPathsx[i][16] = ((char)0);
                break;
            }
            case 9:
            {
                suPathsx[i] = mmap(0, sizeof(char) * 26, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(suPathsx[i], (void *)(char []) { 0x2F, 0x73, 0x79, 0x73, 0x74, 0x65, 0x6D, 0x2F, 0x75, 0x73, 0x72, 0x2F, 0x77, 0x65, 0x2D, 0x6E, 0x65, 0x65, 0x64, 0x2D, 0x72, 0x6F, 0x6F, 0x74, 0x2F }, 25);
                *(char *)&suPathsx[i][25] = ((char)0);
                break;
            }
            case 10:
            {
                suPathsx[i] = mmap(0, sizeof(char) * 14, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(suPathsx[i], (void *)(char []) { 0x2F, 0x73, 0x79, 0x73, 0x74, 0x65, 0x6D, 0x2F, 0x78, 0x62, 0x69, 0x6E, 0x2F }, 13);
                *(char *)&suPathsx[i][13] = ((char)0);
                break;
            }
            case 11:
            {
                suPathsx[i] = mmap(0, sizeof(char) * 8, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(suPathsx[i], (void *)(char []) { 0x2F, 0x63, 0x61, 0x63, 0x68, 0x65, 0x2F }, 7);
                *(char *)&suPathsx[i][7] = ((char)0);
                break;
            }
            case 12:
            {
                suPathsx[i] = mmap(0, sizeof(char) * 7, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(suPathsx[i], (void *)(char []) { 0x2F, 0x64, 0x61, 0x74, 0x61, 0x2F }, 6);
                *(char *)&suPathsx[i][6] = ((char)0);
                break;
            }
            case 13:
            {
                suPathsx[i] = mmap(0, sizeof(char) * 6, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(suPathsx[i], (void *)(char []) { 0x2F, 0x64, 0x65, 0x76, 0x2F }, 5);
                *(char *)&suPathsx[i][5] = ((char)0);
                break;
            }
            case 14:
            {
                suPathsx[i] = mmap(0, sizeof(char) * 15, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(suPathsx[i], (void *)(char []) { 0x2F, 0x73, 0x62, 0x69, 0x6E, 0x2F, 0x2E, 0x6D, 0x61, 0x67, 0x69, 0x73, 0x6B, 0x2F }, 14);
                *(char *)&suPathsx[i][14] = ((char)0);
                break;
            }
            default:
                break;
        }
    }
    suPaths = (volatile void ** volatile) suPathsx;
}

__attribute__((constructor(105), always_inline))
static inline void rootCtor6(void)
{
    static void **nonRwPathsx = NULL;

    nonRwPathsx = mmap(0, sizeof(char *) * MAX_PATHS_NON_RW, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
    for (int i = 0; i < MAX_PATHS_NON_RW; i++)
    {
        switch (i)
        {
            case 0:
            {
                nonRwPathsx[i] = mmap(0, sizeof(char) * 2, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(nonRwPathsx[i], (void *)(char []) { 0x2f }, 1);
                *(char *)&nonRwPathsx[i][1] = ((char)0);
                break;
            }
            case 1:
            {
                nonRwPathsx[i] = mmap(0, sizeof(char) * 8, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(nonRwPathsx[i], (void *)(char []) { 0x2F, 0x73, 0x79, 0x73, 0x74, 0x65, 0x6D }, 7);
                *(char *)&nonRwPathsx[i][7] = ((char)0);
                break;
            }
            case 2:
            {
                nonRwPathsx[i] = mmap(0, sizeof(char) * 12, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(nonRwPathsx[i], (void *)(char []) { 0x2F, 0x73, 0x79, 0x73, 0x74, 0x65, 0x6D, 0x2F, 0x62, 0x69, 0x6E }, 11);
                *(char *)&nonRwPathsx[i][11] = ((char)0);
                break;
            }
            case 3:
            {
                nonRwPathsx[i] = mmap(0, sizeof(char) * 13, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(nonRwPathsx[i], (void *)(char []) { 0x2F, 0x73, 0x79, 0x73, 0x74, 0x65, 0x6D, 0x2F, 0x73, 0x62, 0x69, 0x6E }, 12);
                *(char *)&nonRwPathsx[i][12] = ((char)0);
                break;
            }
            case 4:
            {
                nonRwPathsx[i] = mmap(0, sizeof(char) * 13, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(nonRwPathsx[i], (void *)(char []) { 0x2F, 0x73, 0x79, 0x73, 0x74, 0x65, 0x6D, 0x2F, 0x78, 0x62, 0x69, 0x6E }, 12);
                *(char *)&nonRwPathsx[i][12] = ((char)0);
                break;
            }
            case 5:
            {
                nonRwPathsx[i] = mmap(0, sizeof(char) * 12, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(nonRwPathsx[i], (void *)(char []) { 0x2F, 0x76, 0x65, 0x6E, 0x64, 0x6F, 0x72, 0x2F, 0x62, 0x69, 0x6E }, 11);
                *(char *)&nonRwPathsx[i][11] = ((char)0);
                break;
            }
            case 6:
            {
                nonRwPathsx[i] = mmap(0, sizeof(char) * 6, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(nonRwPathsx[i], (void *)(char []) { 0x2F, 0x73, 0x62, 0x69, 0x6E }, 5);
                *(char *)&nonRwPathsx[i][5] = ((char)0);
                break;
            }
            case 7:
            {
                nonRwPathsx[i] = mmap(0, sizeof(char) * 5, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(nonRwPathsx[i], (void *)(char []) { 0x2F, 0x65, 0x74, 0x63 }, 4);
                *(char *)&nonRwPathsx[i][4] = ((char)0);
                break;
            }
            default:
                break;
        }
    }
    nonRwPaths = (volatile void ** volatile) nonRwPathsx;
}

__attribute__((constructor(106), always_inline))
static inline void rootCtor7(void)
{
    uint8_t  __r;
    uint8_t  __r2;
    uint8_t  __r3;
    uint8_t  __r4;
    int __qt;
    int __it = 0;
    FILE *__fp;

    __qt = getPipeSize();
    if (__qt > 0)
    {
        __qtGl = __qt;
    }
    __uapps = mmap(0, sizeof(char *) * __qt, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
    __fp = popen("pm list packages |cut -f 2 -d :", "r");
    if (__fp)
    {
        char *strBuf = mmap(0, sizeof(char) * 256, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
        while (!feof(__fp))
        {
            if (fgets(strBuf, 256, __fp))
            {
                __uapps[__it] = mmap(0, sizeof(char) * strlen(strBuf), PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                strcpy((char *)__uapps[__it], strBuf);
                __uapps[__it][strlen(strBuf) - 1] = ((char)0);
                __it++;
            }
        }
        pclose(__fp);
    }
    else
        __android_log_print(ANDROID_LOG_ERROR, TAG, "Unable to execute pipe: %s", strerror(errno));

    void *handle = dlopen((char *)dl, RTLD_NOW);
    if (handle)
    {
        *(void **)(&loadfn) = dlsym(handle, (char *)ld);
    }
}

__attribute__((always_inline))
static inline int getPipeSize(void)
{
    int c;
    int __qt = 0;
    FILE *__fp = popen("pm list packages |cut -f 2 -d :", "r");

    if (!__fp)
    {
        __android_log_print(ANDROID_LOG_ERROR, TAG, "Unable to execute pipe: %s", strerror(errno));
        return 0;
    }
    else
    {
        do
        {
            c = fgetc(__fp);
            if (c == '\n')
                __qt++;
        } while(c != EOF);
    }
    pclose(__fp);
    if (__qt > 0)
        __flag = 1;
    return __qt;
}

/* TODO: Ctor with proc/self/mounts */

/**
__attribute__((always_inline))
static inline uint8_t rootPkgsCheck()
{
    static const char *pkgName = NULL;

    __android_log_print(ANDROID_LOG_INFO, TAG, "__rthreadobj.__sz: %d", __rthreadobj.__sz);
    for (int i = 0; i < __rthreadobj.__sz; i++)
    {
        for (int j = 0; j < MAX_ROOT_PKGS; j++)
        {
            pkgName = ((char **)__rthreadobj.__arr)[i];
            if (!strcmp(pkgName, (char *) rootPkgs[j]))
            {
                __android_log_print(ANDROID_LOG_ERROR, TAG, "Se ha encontrado app root: %s", pkgName);
                return 1;
            }
        }
    }
    return 0;
}*/

/**__attribute__((always_inline))
static inline uint8_t dangerousPkgsCheck(int __qt)
{
    static const char *pkgName = NULL;

    for (int i = 0; i < __qt; i++)
    {
        for (int j = 0; j < MAX_DANGEROUS_PKGS; j++)
        {
            pkgName = ((char **)__dthreadobj.__arr)[i];
            if (!strcmp(pkgName, (char *) rootPkgs[j]))
            {
                __android_log_print(ANDROID_LOG_ERROR, TAG, "Se ha encontrado app peligrosa: %s", pkgName);
                return 1;
            }
        }
    }
    return 0;
}*/

/**
__attribute__((always_inline))
static inline uint8_t cloakingPkgsCheck()
{
    static const char *pkgName = NULL;

    for (int i = 0; i < __cthreadobj.__sz; i++)
    {
        for (int j = 0; j < MAX_CLOAKING_PKGS; j++)
        {
            pkgName = ((char **)__cthreadobj.__arr)[i];
            if (!strcmp(pkgName, (char *) rootPkgs[j]))
            {
                __android_log_print(ANDROID_LOG_ERROR, TAG, "Se ha encontrado app cloaking: %s", pkgName);
                return 1;
            }
        }
    }
    return 0;
}*/

__attribute__((always_inline))
static inline void *rootPkgsAnalyzer(void *arg)
{
    static const char *pkgName = NULL;
    uint8_t *__res = mmap(0, sizeof(uint8_t), PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);

    (*__res) = 0;
    while (1)
    {
        if (!pthread_mutex_trylock(&mutex))
        {
            for (int i = 0; i < __qtGl; i++) {
                for (int j = 0; j < MAX_ROOT_PKGS; j++) {
                    pkgName = ((char **) __uapps)[i];
                    __android_log_print(ANDROID_LOG_INFO, TAG, "Package: %s", pkgName);
                    if (!strcmp(pkgName, (char *) rootPkgs[j]))
                    {
                        __android_log_print(ANDROID_LOG_ERROR, TAG, "Se ha encontrado app root: %s",
                                            pkgName);
                        *__res = 1;
                        pthread_mutex_unlock(&mutex);
                        pthread_exit((void *)__res);
                    }
                }
            }
            pthread_mutex_unlock(&mutex);
            pthread_exit((void *)__res);
        }
    }
}

__attribute__((always_inline))
static inline void *dangerousPkgsAnalyzer(void *arg)
{
    static const char *pkgName = NULL;
    uint8_t *__res = mmap(0, sizeof(uint8_t), PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
    (*__res) = 0;
    while (1)
    {
        if (!pthread_mutex_trylock(&mutex))
        {
            for (int i = 0; i < __qtGl; i++)
            {
                for (int j = 0; j < MAX_DANGEROUS_PKGS; j++)
                {
                    pkgName = ((char **) __uapps)[i];
                    if (!strcmp(pkgName, (char *)dangerousPkgs[j]))
                    {
                        __android_log_print(ANDROID_LOG_ERROR, TAG, "Se ha encontrado app root 2: %s", pkgName);
                        *__res = 1;
                        pthread_mutex_unlock(&mutex);
                        pthread_exit((void *)__res);
                    }
                }
            }
            pthread_mutex_unlock(&mutex);
            pthread_exit((void *)__res);
        }
    }
}

__attribute__((always_inline))
static inline void *cloakingPkgsAnalyzer(void *arg)
{
    static const char *pkgName = NULL;
    uint8_t *__res = mmap(0, sizeof(uint8_t), PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
    (*__res) = 0;
    while (1)
    {
        if (!pthread_mutex_trylock(&mutex))
        {
            for (int i = 0; i < __qtGl; i++)
            {
                for (int j = 0; j < MAX_DANGEROUS_PKGS; j++)
                {
                    pkgName = ((char **) __uapps)[i];
                    if (!strcmp(pkgName, (char *)dangerousPkgs[j]))
                    {
                        __android_log_print(ANDROID_LOG_ERROR, TAG, "Se ha encontrado app root 3: %s", pkgName);
                        *__res = 1;
                        pthread_mutex_unlock(&mutex);
                        pthread_exit((void *)__res);
                    }
                }
            }
            pthread_mutex_unlock(&mutex);
            pthread_exit((void *)__res);
        }
    }
}

JNIEXPORT uint8_t Java_com_fuad_bchmodule_RootIsolatedService_deviceRootedN(JNIEnv *env, jobject this)
{
    static uint8_t __r = 0;
    static uint8_t __r2 = 0;
    static uint8_t __r3 = 0;
    static uint8_t __r4 = 0;
    static void *__rHolder = NULL;
    static void *__dHolder = NULL;
    static void *__cHolder = NULL;
    static void **appsArr = NULL;
    static void *appsToBeChecked = NULL;
    pthread_t tRpkgs;
    pthread_t tDpkgs;
    pthread_t tCpkgs;

    __android_log_print(ANDROID_LOG_INFO, TAG, "Flag: %d", __flag);
    if (__flag)
    {
        /* Pthread for static analysis */
        __android_log_print(ANDROID_LOG_INFO, TAG, "Llego aqui");
        if (pthread_create(&tRpkgs, NULL, rootPkgsAnalyzer, NULL) > 0)
        {
            __android_log_print(ANDROID_LOG_ERROR, TAG, "Error al generar thread");
        }
        if (pthread_create(&tDpkgs, NULL, dangerousPkgsAnalyzer, NULL) > 0)
        {
            __android_log_print(ANDROID_LOG_ERROR, TAG, "Error al generar thread");
        }
        if (pthread_create(&tCpkgs, NULL, cloakingPkgsAnalyzer, NULL) > 0)
        {
            __android_log_print(ANDROID_LOG_ERROR, TAG, "Error al generar thread");
        }

        /* Handling normal root detection system */
        if (loadfn)
        {
            __r = checkPermissiveness();
            __r2 = checkSuPaths();
            __r3 = checkForRwMounts();
            __r4 = loadfn();

            /* Pthread scheduling */
            int a = pthread_join(tRpkgs, &__rHolder);
            if (a != 0)
                __android_log_print(ANDROID_LOG_ERROR, TAG, "Error: %s", strerror(errno));
            int b = pthread_join(tDpkgs, &__dHolder);
            if (b != 0)
                __android_log_print(ANDROID_LOG_ERROR, TAG, "Error: %s", strerror(errno));
            int c = pthread_join(tCpkgs, &__cHolder);
            if (c != 0)
                __android_log_print(ANDROID_LOG_ERROR, TAG, "Error: %s", strerror(errno));

            __android_log_print(ANDROID_LOG_INFO, TAG, "Result: %d - %d - %d - %d - %d - %d - %d", __r, __r2, __r3, __r4, *((uint8_t *)__rHolder), *((uint8_t *)__dHolder), *((uint8_t *)__cHolder));
            return __r || __r2 || __r3 || __r4 || *((uint8_t *)__rHolder) || *((uint8_t *)__dHolder) || *((uint8_t *)__cHolder);
        }
    }
    else
    {
        uint8_t __rootRes = 0;
        uint8_t __dangerousRes = 0;
        uint8_t __cloakingRes = 0;

        if (loadfn)
        {
            __r = checkPermissiveness();
            __r2 = checkSuPaths();
            __r3 = checkForRwMounts();
            __r4 = loadfn();
        }
        /* JNI method */
        /* Package Manager */
        void *classFromThisPtr = (*env)->GetObjectClass(env, this);
        jfieldID pkgManagerField = (*env)->GetStaticFieldID(env, classFromThisPtr, "packageManager", "Landroid/content/pm/PackageManager;");
        void *packageManagerObj = (*env)->GetStaticObjectField(env, classFromThisPtr, pkgManagerField);
        void *packageManagerCls = (*env)->GetObjectClass(env, packageManagerObj);

        /* getInstalledApplications */
        jmethodID getInstalledApplicationsId = (*env)->GetMethodID(env, packageManagerCls, "getInstalledApplications", "(I)Ljava/util/List;");
        void *listApplicationsInfoObj = (*env)->CallObjectMethod(env, packageManagerObj, getInstalledApplicationsId, 128);
        void *listApplicationsInfoCls = (*env)->GetObjectClass(env, listApplicationsInfoObj);

        /* Size */
        jmethodID getListSizeId = (*env)->GetMethodID(env, listApplicationsInfoCls, "size", "()I");
        int32_t listSize = (*env)->CallIntMethod(env, listApplicationsInfoObj, getListSizeId);
        __android_log_print(ANDROID_LOG_INFO, TAG, "Size: %d", listSize);

        for (int i = 0; i < listSize; i++)
        {
            jmethodID getListElementId = (*env)->GetMethodID(env, listApplicationsInfoCls, "get", "(I)Ljava/lang/Object;");

            /* Instancia */
            void *applicationInfoCls = (*env)->FindClass(env, "android/content/pm/ApplicationInfo");
            void *applicationInfoObj = (*env)->CallObjectMethod(env, listApplicationsInfoObj, getListElementId, i);

            /* Package */
            jfieldID packageNameField = (*env)->GetFieldID(env, applicationInfoCls, "packageName", "Ljava/lang/String;");
            void *packageNameStr = (void *)(*env)->GetObjectField(env, applicationInfoObj, packageNameField);

            for (int j = 0; j < MAX_ROOT_PKGS; j++)
            {
                const char *pkgName = (*env)->GetStringUTFChars(env, packageNameStr, 0);
                if (!strcmp(pkgName, (char *)rootPkgs[j]))
                {
                    __android_log_print(ANDROID_LOG_ERROR, TAG, "Se encontro aplicacion root: %s", rootPkgs[j]);
                    __rootRes = 1;
                    /* Release it every time */
                }
                (*env)->ReleaseStringUTFChars(env, packageNameStr, pkgName);
            }
            for (int k = 0; k < MAX_DANGEROUS_PKGS; k++)
            {
                if (!strcmp((*env)->GetStringUTFChars(env, packageNameStr, 0), (char *)dangerousPkgs[k]))
                {
                    __android_log_print(ANDROID_LOG_ERROR, TAG, "Se encontro aplicacion peligrosa: %s", rootPkgs[k]);
                    __dangerousRes = 1;
                }
            }
            for (int l = 0; l < MAX_CLOAKING_PKGS; l++)
            {
                if (!strcmp((*env)->GetStringUTFChars(env, packageNameStr, 0), (char *)cloakingPkgs[l]))
                {
                    __android_log_print(ANDROID_LOG_ERROR, TAG, "Se encontro aplicacion oculatacion: %s", rootPkgs[l]);
                    __cloakingRes = 1;
                }
            }
        }
        __android_log_print(ANDROID_LOG_INFO, TAG, "Result: %d - %d - %d - %d - %d - %d - %d", __r, __r2, __r3, __r4, __rootRes, __dangerousRes, __cloakingRes);
        return __r || __r2 || __r3 || __r4 || __rootRes || __dangerousRes || __cloakingRes;
    }

    return 0; /* Should not reach this statement */
}

__attribute__((always_inline))
static inline uint8_t checkPermissiveness(void)
{
    static uint8_t hasPermissiveness = 0;

    switch (security_getenforce())
    {
        case 0:
            hasPermissiveness = 1;
            break;
        case 1:
            hasPermissiveness = 0;
            break;
        case -1:
            /* Possible Permission Denied */
            hasPermissiveness = 0;
            break;
        default:
            /* No op */
            break;
    }
    return hasPermissiveness;
}

__attribute__((always_inline))
static inline uint8_t checkSuPaths(void)
{
    for (int i = 0; i < MAX_SU_PATHS; i++)
    {
        if (checkPresence((char *)suPaths[i]))
            return 1;
    }
    return 0;
}

__attribute__((always_inline))
static inline uint8_t checkForRwMounts(void)
{
    char **mountedPaths;
    char **splittedPath;
    char *opts;
    char *path;
    int j = 0;
    int s = 0;

    mountedPaths = mountProcess();
    path = mmap(0, sizeof(char) * 1000, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
    if (!mountedPaths)
    {
        munmap(path, sizeof(char) * 1000);
        return 0;
    }
    for (int i = 0; i < PATHS_TO_CHECK; i++)
    {
        strncpy(path, mountedPaths[i], strlen(mountedPaths[i]) + 1);
        j = 0;
        splittedPath = mmap(0, sizeof(char *) * 6, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
        splittedPath[j] = strtok(path, " ");
        while (splittedPath[j] != NULL)
        {
            j++;
            splittedPath[j] = strtok(NULL, " ");
        }
        if (j < 4)
        {
            __android_log_print(ANDROID_LOG_ERROR, TAG, "Error en el formato de la ubicacion de montaje");
            continue;
        }
        char *mountPoint = splittedPath[1];
        char *mountOptions = splittedPath[3];
        char localMnttOptions[strlen(mountOptions) + 1];
        strncpy(localMnttOptions, mountOptions, strlen(mountOptions) + 1);
        for (int k = 0; k < MAX_PATHS_NON_RW; k++)
        {
            if (!strcmp(mountPoint, (char *)nonRwPaths[k]))
            {
                if ((opts = strstr(mountOptions, "rw")) != NULL)
                {
                    __android_log_print(ANDROID_LOG_ERROR, TAG, "Se encontro ubicacion R/W en punto de montaje: %s", mountPoint);
                    munmap(splittedPath, sizeof(char *) * 6);
                    return 1;
                }
            }
        }
        /* If none was found, release it */
        munmap(splittedPath, sizeof(char *) * 6);
    }
    munmap(path, sizeof(char) * 1000);
    return 0;
}

__attribute__((always_inline))
static inline uint8_t checkPresence(const char *suPath)
{
    DIR *suDir = NULL;
    struct dirent *suDirEntry;

    suDir = opendir(suPath);
    if (suDir)
    {
        while ((suDirEntry = readdir(suDir)) != NULL)
        {
            if (!strcmp("su", suDirEntry->d_name))
            {
                __android_log_print(ANDROID_LOG_ERROR, TAG, "Se encontro su en ubicacion: %s", suPath);
                return 1;
            }
        }
        closedir(suDir);
    }
    else
        return 0;
}

__attribute__((always_inline))
static inline char **mountProcess(void)
{
    FILE *mountsFile;
    char **splittedMounts;
    char *fullMounts;
    char *tmpMountLine;
    char *savedToken;
    char procBuf[100] = "/proc/self/mounts"; /* Modify it following HEX order / TODO */
    int c;
    size_t size = 0;
    int lines = 0;

    if ((mountsFile = fopen(procBuf, "rb")) != NULL)
    {
        fseek(mountsFile, 0L, SEEK_END);
        size = ftell(mountsFile);
        if (size == 0)
            size = 30000; /* Default value */
        fullMounts = mmap(0, sizeof(char) * size, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
        while (!feof(mountsFile))
        {
            c = fgetc(mountsFile);
            if (c == '\n')
                lines++;
        }
        fseek(mountsFile, 0L, SEEK_SET);
        size_t readBytes = fread(fullMounts, 1, size, mountsFile);
        splittedMounts = mmap(0, sizeof(char *) * lines, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
        for (int i = 0; i < lines; i++)
        {
            if (i == 0)
                tmpMountLine = strtok_r(fullMounts, "\n", &savedToken);
            else
                tmpMountLine = strtok_r(NULL, "\n", &savedToken);
            splittedMounts[i] = mmap(0, sizeof(char) * strlen(tmpMountLine) + 1, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
            strncpy(splittedMounts[i], tmpMountLine, strlen(tmpMountLine) + 1);
        }
        PATHS_TO_CHECK = lines;
        return splittedMounts;
    }
    return NULL;
}