/**
 * TODO: Añadir syscalls para evitar uso de libc
 */
#include <selinux/selinux.h>
#include <selinux/avc.h>
#include "magisk.h"

/* Variables globales */
static const int MAX_MAGISK_PATH = 4;
static const char *TAG = "MagiskLibDetector";
static uint8_t wasFound;
/* Variables globales utilizadas en las funciones */
static volatile void ** volatile magiskBlacklistedPath;
static volatile void * volatile defaultSbinDirectory;
static volatile void * volatile defaultDirectory;
static volatile void * volatile keyword;


__attribute__((constructor(100), always_inline))
static inline void magiskCtor(void)
{
    /* Variables que contienen los strings / Siempre es cantidad caracteres + 1 en mmap (Segundo parámetro) */
    static void *defaultSbinDir = NULL;
    static void *defaultDir = NULL;
    static void *kw = NULL;
    static void **blackList = NULL;

    /* Solicitud de memoria */
    defaultSbinDir = mmap(0, sizeof(char) * 7, PROT_READ | PROT_WRITE,
            MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
    defaultDir = mmap(0, sizeof(char) * 16, PROT_READ | PROT_WRITE,
            MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
    kw = mmap(0, sizeof(char) * 7, PROT_READ | PROT_WRITE,
            MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
    blackList = mmap(0, sizeof(char *) * MAX_MAGISK_PATH, PROT_READ | PROT_WRITE,
            MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
    /* Función que asigna los strings */
    setupEnvironment(defaultSbinDir, defaultDir, kw, blackList);
    /* Copia de strings a variables globales */
    defaultSbinDirectory = (volatile void * volatile)defaultSbinDir;
    defaultDirectory = (volatile void * volatile)defaultDir;
    keyword = (volatile void * volatile)kw;
    magiskBlacklistedPath = (volatile void ** volatile)blackList;
}

/* Esto no importa */
__attribute__((constructor(101), always_inline))
static inline void magiskCtor2(void)
{
    uint8_t mounts = lookForMounts();
    uint8_t ctx = lookForContexts();
    wasFound = (mounts || ctx);
}

__attribute__((always_inline))
static inline void setupEnvironment(void *defaultSbinDir, void *defaultDir, void *kw, void **blackList)
{
    /* Normal pointers initialization / El copy_amount es siempre el valor de la cantidad de caracteres del string (real) */
    memcpy(defaultSbinDir, (void *)(char []) { 0x2F, 0x73, 0x62, 0x69, 0x6E, 0x2F }, 6);
    memcpy(defaultDir, (void *)(char []) { 0x2F, 0x70, 0x72, 0x6F, 0x63, 0x2F, 0x25, 0x64, 0x2F,
                                           0x6D, 0x6F, 0x75, 0x6E, 0x74, 0x73 }, 15);
    memcpy(kw, (void *)(char []) { 0x6D, 0x61, 0x67, 0x69, 0x73, 0x6B }, 6);

    *(char *)&defaultSbinDir[6] = ((char)0);
    *(char *)&defaultDir[15] = ((char)0);
    *(char *)&kw[6] = ((char)0);

    /* Double pointers initialization */
    for (int i = 0; i < MAX_MAGISK_PATH; i++)
    {
        switch (i)
        {
            case 0:
            {
                blackList[i] = mmap(0, sizeof(char) * 15, PROT_READ | PROT_WRITE,
                        MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(blackList[i], (void *)(char []) { 0x2F, 0x73, 0x62, 0x69, 0x6E, 0x2F,
                                                         0x2E, 0x6D, 0x61, 0x67, 0x69, 0x73, 0x6B,
                                                         0x2F }, 14);
                *(char *)&blackList[i][14] = ((char)0);
                break;
            }
            case 1:
            {
                blackList[i] = mmap(0, sizeof(char) * 19, PROT_READ | PROT_WRITE,
                        MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(blackList[i], (void *)(char []) { 0x2F, 0x73, 0x62, 0x69, 0x6E, 0x2F,
                                                         0x2E, 0x63, 0x6F, 0x72, 0x65, 0x2F, 0x6D,
                                                         0x69, 0x72, 0x72, 0x6F, 0x72 }, 18);
                *(char *)&blackList[i][18] = ((char)0);
                break;
            }
            case 2:
            {
                blackList[i] = mmap(0, sizeof(char) * 16, PROT_READ | PROT_WRITE,
                        MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(blackList[i], (void *)(char []) { 0x2F, 0x73, 0x62, 0x69, 0x6E, 0x2F,
                                                         0x2E, 0x63, 0x6F, 0x72, 0x65, 0x2F, 0x69,
                                                         0x6D, 0x67 }, 15);
                *(char *)&blackList[i][15] = ((char)0);
                break;
            }
            case 3:
            {
                blackList[i] = mmap(0, sizeof(char) * 27, PROT_READ | PROT_WRITE,
                        MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                memcpy(blackList[i], (void *)(char []) { 0x2F, 0x73, 0x62, 0x69, 0x6E, 0x2F,
                                                         0x2E, 0x63, 0x6F, 0x72, 0x65, 0x2F, 0x64,
                                                         0x62, 0x2D, 0x30, 0x2F, 0x6D, 0x61, 0x67,
                                                         0x69, 0x73, 0x6B, 0x2E, 0x64, 0x62 }, 26);
                *(char *)&blackList[i][26] = ((char)0);
                break;
            }
            default:
                __android_log_print(ANDROID_LOG_WARN, TAG, "Comportamiento inesperado");
                break;
        }
    }
}

__attribute__((always_inline))
static inline uint8_t lookForMounts(void)
{
    FILE *mounts = NULL;
    char *lines = NULL;
    char *substr = NULL;
    char buffer[100];
    uint8_t mountFound = 0;
    long byteSize = 0;

    memset(buffer, ((char)0), 100);
    sprintf(buffer, (char *)defaultDirectory, getpid());
    if ((mounts = fopen(buffer, "r")) == NULL)
    {
        __android_log_print(ANDROID_LOG_ERROR, TAG, "Error al abrir mounts");
        return mountFound;
    }
    fseek(mounts, 0L, SEEK_END);
    byteSize = ftell(mounts);
    if (!byteSize)
        byteSize = 3000; /* Default value */
    lines = mmap(0, sizeof(char) * byteSize, PROT_READ | PROT_WRITE,
            MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
    if (!lines)
    {
        fclose(mounts);
        return mountFound;
    }
    size_t readBytes = fread(lines, 1, byteSize, mounts);
    for (int i = 0; i < MAX_MAGISK_PATH; i++)
    {
        if ((substr = strstr(lines, (char *)magiskBlacklistedPath[i])) != NULL)
        {
            __android_log_print(ANDROID_LOG_ERROR, TAG, "Punto de montaje de Magisk: %s", (volatile char * volatile)magiskBlacklistedPath[i]);
            mountFound = 1;
            munmap(lines, sizeof(char) * byteSize);
            fclose(mounts);
            break;
        }
    }
    return mountFound;
}

__attribute__((always_inline))
static inline uint8_t lookForContexts(void)
{
    DIR *rootDirectory = NULL;
    struct dirent *dirIterator = NULL;
    char *path;
    security_context_t dirCtx;
    uint8_t ctxFound = 0;

    rootDirectory = opendir((char *)defaultSbinDirectory);
    if (!rootDirectory && (errno == EACCES))
    {
        return ctxFound;
    }
    else if (!rootDirectory && (errno != EACCES))
    {
        __android_log_print(ANDROID_LOG_ERROR, TAG, "Error al abrir directorio: %s", strerror(errno));
        return ctxFound;
    }
    else
    {
        while ((dirIterator = readdir(rootDirectory)) != NULL)
        {
            if (!strcmp(dirIterator->d_name, ".") || !strcmp(dirIterator->d_name, ".."))
                continue;
            else
            {
                path = mmap(0, sizeof(char) * 200, PROT_READ | PROT_WRITE,
                        MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
                strcpy(path, (char *)defaultSbinDirectory);
                if (getfilecon(strcat(path, dirIterator->d_name), &dirCtx) > 0)
                {
                    if (strstr(dirCtx, (char *)keyword) != NULL)
                    {
                        __android_log_print(ANDROID_LOG_ERROR, TAG, "Contexto de Magisk: %s", dirCtx);
                        freecon(dirCtx);
                        munmap(path, sizeof(char) * 200);
                        ctxFound = 1;
                        break;
                    }
                }
            }
        }
        closedir(rootDirectory);
    }
    return ctxFound;
}

__attribute__((visibility("default"), always_inline))
inline uint8_t load()
{
    return wasFound;
}