#ifndef MAGISK_H
#define MAGISK_H

#include <unistd.h>
#include <jni.h>
#include <unistd.h>
#include <malloc.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <android/log.h>
#include <errno.h>
#include <sys/mman.h>

__attribute__((constructor(100), always_inline)) static inline void magiskCtor();
__attribute__((constructor(101), always_inline)) static inline void magiskCtor2();
__attribute__((always_inline)) static inline void setupEnvironment(void *, void *, void *, void **);
__attribute__((always_inline)) static inline uint8_t lookForMounts(void);
__attribute__((always_inline)) static inline uint8_t lookForContexts(void);
extern __attribute__((visibility("default"), always_inline)) inline uint8_t load(void);

#endif /* MAGISK_H */
