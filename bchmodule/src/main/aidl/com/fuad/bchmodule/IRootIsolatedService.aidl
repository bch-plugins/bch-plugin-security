// IRootIsolatedService.aidl
package com.fuad.bchmodule;

// Declare any non-default types here with import statements

interface IRootIsolatedService {
    boolean deviceRooted();
}
