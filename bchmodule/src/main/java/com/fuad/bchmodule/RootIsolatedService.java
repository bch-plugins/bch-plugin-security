package com.fuad.bchmodule;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.util.Log;

public class RootIsolatedService extends Service {

   static {
        System.loadLibrary("root");
   }

   public native boolean deviceRootedN();

   private static final String TAG = "RootLibDetector";
   private static String[] appsToBeChecked = null;
   private Context context;
   private static PackageManager packageManager;

   @Override
   public IBinder onBind(Intent intent) {
       return rootIsolatedBinder;
   }

   private final IRootIsolatedService.Stub rootIsolatedBinder = new IRootIsolatedService.Stub() {
        public boolean deviceRooted() {
            boolean result = false;
            int size = 0;
            long start = System.currentTimeMillis();

            context = getApplicationContext();

            packageManager = context.getPackageManager();
            Log.d(TAG, "Comprobando dispositivo");
            result = deviceRootedN();
            Log.d(TAG, "Dispositivo: " + result);
            Log.d(TAG, "Time: " + ((System.currentTimeMillis() - start) / 1000) + " secs");
            return result;
        }
    };
}
