package com.fuad.root;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Button;

import com.fuad.bchmodule.IRootIsolatedService;
import com.fuad.bchmodule.RootIsolatedService;

public class MainActivity extends AppCompatActivity {

    static {
        System.loadLibrary("frida");
    }

    private static final String TAG = "RootLibDetector";
    private IRootIsolatedService rootBinder;
    private boolean isRootBound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btn = findViewById(R.id.button);
        btn.setOnClickListener((v) -> {
            if (isRootBound) {
                try {
                    rootBinder.deviceRooted();
                } catch (RemoteException e) {
                    Log.e(TAG, "Error: " + e.getMessage(), e);
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart()");
        Intent rootIntent = new Intent(this, RootIsolatedService.class);
        getApplicationContext().bindService(rootIntent, rootServiceConnectionBinder, BIND_AUTO_CREATE);
    }

    private final ServiceConnection rootServiceConnectionBinder = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            rootBinder = IRootIsolatedService.Stub.asInterface(service);
            isRootBound = true;
            Log.d(TAG, "Inicio de servicio: enlazado");
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isRootBound = false;
            Log.d(TAG, "Fin de servicio: desenlazado");
        }
    };
}